﻿using SendGrid;
using System.Net;
using System.Threading.Tasks;
namespace DesafioMegaWeb.Service
{
    public class ServicoEmail
    {
        public static Task EnviaEmailAsync(string email, string assunto, string mensagem)
        {
            // serviço para enviar email
            var minhaMensagem = new SendGridMessage();
            minhaMensagem.AddTo(email);
            minhaMensagem.From = new System.Net.Mail.MailAddress("charlliequadros@gmail.com", "DesafioMegaWeb");
            minhaMensagem.Subject = assunto;
            minhaMensagem.Text = mensagem;
            minhaMensagem.Html = mensagem;

            var credenciais = new NetworkCredential("charlliequadros", "animai-vos1");
            // Cria um transporte web para enviar email
            var transporteWeb = new Web(credenciais);
            // Envia o email
            if (transporteWeb != null)
            {
                return transporteWeb.DeliverAsync(minhaMensagem);
            }
            else
            {
                return Task.FromResult(0);
            }
        }
    }
}