﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DesafioMegaWeb.Startup))]
namespace DesafioMegaWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
