namespace DesafioMegaWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDadosModels : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BlocosModels", "LevatamentoModel_Id", c => c.Int());
            AddColumn("dbo.LuminariasModels", "PavimentosModel_Id", c => c.Int());
            AddColumn("dbo.PavimentosModels", "Nome", c => c.String());
            AddColumn("dbo.PavimentosModels", "Descricao", c => c.String());
            AddColumn("dbo.PavimentosModels", "BlocosModel_Id", c => c.Int());
            CreateIndex("dbo.BlocosModels", "LevatamentoModel_Id");
            CreateIndex("dbo.PavimentosModels", "BlocosModel_Id");
            CreateIndex("dbo.LuminariasModels", "PavimentosModel_Id");
            AddForeignKey("dbo.LuminariasModels", "PavimentosModel_Id", "dbo.PavimentosModels", "Id");
            AddForeignKey("dbo.PavimentosModels", "BlocosModel_Id", "dbo.BlocosModels", "Id");
            AddForeignKey("dbo.BlocosModels", "LevatamentoModel_Id", "dbo.LevatamentoModels", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BlocosModels", "LevatamentoModel_Id", "dbo.LevatamentoModels");
            DropForeignKey("dbo.PavimentosModels", "BlocosModel_Id", "dbo.BlocosModels");
            DropForeignKey("dbo.LuminariasModels", "PavimentosModel_Id", "dbo.PavimentosModels");
            DropIndex("dbo.LuminariasModels", new[] { "PavimentosModel_Id" });
            DropIndex("dbo.PavimentosModels", new[] { "BlocosModel_Id" });
            DropIndex("dbo.BlocosModels", new[] { "LevatamentoModel_Id" });
            DropColumn("dbo.PavimentosModels", "BlocosModel_Id");
            DropColumn("dbo.PavimentosModels", "Descricao");
            DropColumn("dbo.PavimentosModels", "Nome");
            DropColumn("dbo.LuminariasModels", "PavimentosModel_Id");
            DropColumn("dbo.BlocosModels", "LevatamentoModel_Id");
        }
    }
}
