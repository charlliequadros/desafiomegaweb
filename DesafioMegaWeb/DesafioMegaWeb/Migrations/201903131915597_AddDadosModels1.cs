namespace DesafioMegaWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDadosModels1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LuminariasModels", "Descricao", c => c.String());
            AddColumn("dbo.LuminariasModels", "Nome", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.LuminariasModels", "Nome");
            DropColumn("dbo.LuminariasModels", "Descricao");
        }
    }
}
