﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DesafioMegaWeb.Models
{
    public class LevatamentoModel
    {
        public int Id { get; set; }

        public virtual List<BlocosModel> Blocos{ get; set; }
    }
}