﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DesafioMegaWeb.Models
{
    public partial class PavimentosModel
    {
        public int Id { get; set; }

        public string Nome { get; set; }
        public string Descricao { get; set; }

        public virtual List<LuminariasModel> Luminarias { get; set; }

    }
}