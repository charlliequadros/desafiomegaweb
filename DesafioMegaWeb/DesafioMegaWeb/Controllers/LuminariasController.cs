﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DesafioMegaWeb.Models;

namespace DesafioMegaWeb.Controllers
{
    public class LuminariasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Luminarias
        [Authorize]
        public ActionResult Index()
        {
            return View(db.LuminariasModels.ToList());
        }

        // GET: Luminarias/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LuminariasModel luminariasModel = db.LuminariasModels.Find(id);
            if (luminariasModel == null)
            {
                return HttpNotFound();
            }
            return View(luminariasModel);
        }

        // GET: Luminarias/Create
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Luminarias/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "Id,Nome,Descricao")] LuminariasModel luminariasModel)
        {
            if (ModelState.IsValid)
            {
                db.LuminariasModels.Add(luminariasModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(luminariasModel);
        }

        // GET: Luminarias/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LuminariasModel luminariasModel = db.LuminariasModels.Find(id);
            if (luminariasModel == null)
            {
                return HttpNotFound();
            }
            return View(luminariasModel);
        }

        // POST: Luminarias/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nome,Descricao")] LuminariasModel luminariasModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(luminariasModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(luminariasModel);
        }

        // GET: Luminarias/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LuminariasModel luminariasModel = db.LuminariasModels.Find(id);
            if (luminariasModel == null)
            {
                return HttpNotFound();
            }
            return View(luminariasModel);
        }

        // POST: Luminarias/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LuminariasModel luminariasModel = db.LuminariasModels.Find(id);
            db.LuminariasModels.Remove(luminariasModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
