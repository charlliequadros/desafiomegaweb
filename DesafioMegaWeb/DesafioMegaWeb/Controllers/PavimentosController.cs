﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DesafioMegaWeb.Models;
using System.Globalization;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;


namespace DesafioMegaWeb.Controllers
{
    public class PavimentosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        private List<LuminariasModel> luminarias = new List<LuminariasModel>();
        // GET: Pavimentos
        [Authorize]
        public ActionResult Index()
        {
            var tt = db.PavimentosModels.ToList();
            return View(db.PavimentosModels.ToList());
        }

        // GET: Pavimentos/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PavimentosModel pavimentosModel = db.PavimentosModels.Find(id);
            if (pavimentosModel == null)
            {
                return HttpNotFound();
            }
            return View(pavimentosModel);
        }

        // GET: Pavimentos/Create
        [Authorize]
        public ActionResult Create()
        {


            var model = new PavimentosModel(){Luminarias = new List<LuminariasModel>(),idLuminaria = new List<int>()};
            var pavimentos = db.PavimentosModels
                .Where(x => x.Luminarias.Count > 0)
                .SelectMany(x => x.Luminarias)
                .ToList();

            luminarias = db.LuminariasModels.ToList().Where(x=>!pavimentos.Any(y=>y.Id==x.Id)).ToList(); 
            ViewBag.Luminarias = new MultiSelectList(luminarias, "Id", "Nome","Descricao");
            return View(model);
        }

        // POST: Pavimentos/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PavimentosModel pavimentosModel)
        {
            foreach (var item in pavimentosModel.idLuminaria)
            {
                pavimentosModel.Luminarias.Add(luminarias.FirstOrDefault(x=>x.Id==item));
            }
            if (ModelState.IsValid)
            {
                db.PavimentosModels.Add(pavimentosModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pavimentosModel);
        }

        // GET: Pavimentos/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PavimentosModel pavimentosModel = db.PavimentosModels.Find(id);
            if (pavimentosModel == null)
            {
                return HttpNotFound();
            }
            return View(pavimentosModel);
        }

        // POST: Pavimentos/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit([Bind(Include = "Id")] PavimentosModel pavimentosModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pavimentosModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pavimentosModel);
        }

        // GET: Pavimentos/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PavimentosModel pavimentosModel = db.PavimentosModels.Find(id);
            if (pavimentosModel == null)
            {
                return HttpNotFound();
            }
            return View(pavimentosModel);
        }

        // POST: Pavimentos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DeleteConfirmed(int id)
        {
            PavimentosModel pavimentosModel = db.PavimentosModels.Find(id);
            db.PavimentosModels.Remove(pavimentosModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
