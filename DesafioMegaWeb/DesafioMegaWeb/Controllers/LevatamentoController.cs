﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DesafioMegaWeb.Models;

namespace DesafioMegaWeb.Controllers
{
    public class LevatamentoController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Levatamento
        [Authorize]
        public ActionResult Index()
        {
            var teste = db.LevatamentoModels.ToList();
            return View(db.LevatamentoModels.ToList());
        }

        // GET: Levatamento/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LevatamentoModel levatamentoModel = db.LevatamentoModels.Find(id);
            if (levatamentoModel == null)
            {
                return HttpNotFound();
            }
            return View(levatamentoModel);
        }

        // GET: Levatamento/Create
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Levatamento/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "Id")] LevatamentoModel levatamentoModel)
        {
            if (ModelState.IsValid)
            {
                db.LevatamentoModels.Add(levatamentoModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(levatamentoModel);
        }

        // GET: Levatamento/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LevatamentoModel levatamentoModel = db.LevatamentoModels.Find(id);
            if (levatamentoModel == null)
            {
                return HttpNotFound();
            }
            return View(levatamentoModel);
        }

        // POST: Levatamento/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id")] LevatamentoModel levatamentoModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(levatamentoModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(levatamentoModel);
        }

        // GET: Levatamento/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LevatamentoModel levatamentoModel = db.LevatamentoModels.Find(id);
            if (levatamentoModel == null)
            {
                return HttpNotFound();
            }
            return View(levatamentoModel);
        }

        // POST: Levatamento/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LevatamentoModel levatamentoModel = db.LevatamentoModels.Find(id);
            db.LevatamentoModels.Remove(levatamentoModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
